public class Kurs {

    public Kurs(int numer, int iloscTowarow, int iloscOsob) {
        this.numer = numer;
        this.iloscTowarow = iloscTowarow;
        this.iloscOsob = iloscOsob;
    }

    private int numer;
    private int iloscTowarow;
    private int iloscOsob;

    public int getNumer() {
        return numer;
    }

    public void setNumer(int numer) {
        this.numer = numer;
    }

    public int getIloscTowarow() {
        return iloscTowarow;
    }

    public void setIloscTowarow(int iloscTowarow) {
        this.iloscTowarow = iloscTowarow;
    }

    public int getIloscOsob() {
        return iloscOsob;
    }

    public void setIloscOsob(int iloscOsob) {
        this.iloscOsob = iloscOsob;
    }
}
