import java.util.ArrayList;
import java.util.List;

public class FabrykaPojazdow {

    public List<Pojazd> pudujPojazdy(){

        List<Pojazd> listaPojazdow = new ArrayList<Pojazd>();
        listaPojazdow.add(new Pojazd("Czerwony", TypPojazdu.OSOBOWY, 100, 5));
        listaPojazdow.add(new Pojazd("Niebieski", TypPojazdu.DOSTAWCZY, 3500, 3));
        listaPojazdow.add(new Pojazd("Bialy", TypPojazdu.DOSTAWCZY, 3200, 2));
        listaPojazdow.add(new Pojazd("Zolty", TypPojazdu.OSOBOWY, 50, 2));
        listaPojazdow.add(new Pojazd("Zielony", TypPojazdu.OSOBOWY, 250, 5));
        listaPojazdow.add(new Pojazd("Czarny", TypPojazdu.OSOBOWY, 600, 3));

        return listaPojazdow;
    }
}
