import java.util.ArrayList;
import java.util.List;

public class Dyspozytor {

    FabrykaPojazdow fabrykaPojazdow = new FabrykaPojazdow();


    public void rozdzielKursy() {

        List<Pojazd> pojazdy = fabrykaPojazdow.pudujPojazdy();
        List<Kurs> kursy = zestawienieKursow();

        kursy.forEach(kurs -> {
            int iloscOsob = kurs.getIloscOsob();
            int iloscTowarow = kurs.getIloscTowarow();
            for (Pojazd pojazd : pojazdy ) {
                if (pojazd.sprawdzCzyMozeszZabracPasazerow(iloscOsob) && pojazd.sprawdzCzyMozeszZabracTowar(iloscTowarow)){
                    System.out.println("Pojazd " + pojazd.getTypPojazdu() + " w kolorze " + pojazd.getKolor() + " bierze kurs numer " + kurs.getNumer());
                    pojazdy.remove(pojazd);
                    return;
                }
            }

        });

    }

    private List<Kurs> zestawienieKursow() {

        ArrayList<Kurs> kursy = new ArrayList<Kurs>();
        kursy.add(new Kurs(2, 3400, 3));
        kursy.add(new Kurs(1, 100, 5));
        kursy.add(new Kurs(3, 3200, 2));
        kursy.add(new Kurs(5, 250, 4));
        kursy.add(new Kurs(4, 50, 2));
        kursy.add(new Kurs(6, 600, 2));

        return kursy;
    }

}

