/**
 * To jest obiekt Pojazd który ma 4 pola które opisują ilość towaru jaki może zabrać, kolor i typ pojazdu.
 */

public class Pojazd {

    /**
     * Standardowy konstuktor do którego przkazujemy informacje o pojeździe który chcemy zbudować
     */
    public Pojazd(String kolor, TypPojazdu typPojazdu, int ladownosc, int maxIloscPasazerow) {
        this.kolor = kolor;
        this.typPojazdu = typPojazdu;
        this.ladownosc = ladownosc;
        this.maxIloscPasazerow = maxIloscPasazerow;
    }

    private String kolor;
    private TypPojazdu typPojazdu;
    private int ladownosc;
    private int maxIloscPasazerow;

    /**
     * To jest metoda dzięki której pojazd stwierdza czy może zabrać tylu paseżerów ile podajemy w parametrze.
     */
    public boolean sprawdzCzyMozeszZabracPasazerow(int iloscPasazerowDoZabrania){
        return maxIloscPasazerow >= iloscPasazerowDoZabrania;
    }

    /**
     * To jest metoda dzięki której pojazd stwierdza czy może zabrać tylu paseżerów ile podajemy w parametrze.
     */
    public boolean sprawdzCzyMozeszZabracTowar(int iloscTowaruDoZabrania){
        return ladownosc >= iloscTowaruDoZabrania;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public TypPojazdu getTypPojazdu() {
        return typPojazdu;
    }

    public void setTypPojazdu(TypPojazdu typPojazdu) {
        this.typPojazdu = typPojazdu;
    }

    public int getLadownosc() {
        return ladownosc;
    }

    public void setLadownosc(int ladownosc) {
        this.ladownosc = ladownosc;
    }

    public int getMaxIloscPasazerow() {
        return maxIloscPasazerow;
    }

    public void setMaxIloscPasazerow(int maxIloscPasazerow) {
        this.maxIloscPasazerow = maxIloscPasazerow;
    }
}
